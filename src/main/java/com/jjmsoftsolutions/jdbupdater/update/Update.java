package com.jjmsoftsolutions.jdbupdater.update;

public interface Update {
	public String execute();
}
