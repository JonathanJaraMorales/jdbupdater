package com.jjmsoftsolutions.jdbupdater.update;

public interface DataBase {
	
	public void setUrl(String url);
	public String getUrl();
	
	public void setPassword(String password);
	public String getPassword();
	
	public void setDriver(String driver);
	public String getDriver();
	
	public void setUser(String user);
	public String getUser();
}
